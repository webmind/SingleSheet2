## How this works

### Storage

This version stores the entire sheet into a json-blob in the brownsers local-storage. 

Storing seperate json-blobs on one browser is being considered.

The structure of the data blob follows a Chapter, Section, Field structure, Each field is part of section, which is part of a chapter. Some section chapters can have only one section with only one field. 

A field consists of a label, a comment and a value, but not all need to be used. For instance, for attributes, comments don't really make a lot of sense, so it doesn't get used/rendered

### Export & Import

The json blob can be exported and imported.

### Page generation

Page generation happens based on the json-blob from local storage, if a new sheet is created first a new json-blob is filled and then the page gets regenerated. 

### Loading & Saving

Saving happens on change, the onchange action typically happens when the field loses focus, so pressing <tab> is usually sufficient

Loading happens on refresh, put a reload from localstorage button is planned. 

